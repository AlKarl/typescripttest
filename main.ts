let num1:number;
let num2:number;

const multiply = (num1:number, num2:number) => {
    console.log(num1+num2);
}

multiply(2, 7);


interface Person {
    name: string;
    age: number;
}

let person: Person;
person.name = 'Uno';
person.age = 63;

console.log(person.name);
console.log(person.age);

enum Type {
    CRE = 'CREATE',
    UPD = 'UPDATE',
    DEL = 'DELETE',
    ERR = 'ERROR',
}

let type: Type;
type = Type.CRE;

console.log('Type:', type);

